# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations

def migrate_usersocs(apps, schema_editor, backwards=False):
    UserSocialMedia = apps.get_model("person", "UserSocialMedia")
    for st in UserSocialMedia.objects.all():
        if '@' not in st.socid[1:] or not st.socid.startswith('@'):
            continue
        # Reversed data needs to be corrected.
        (site_id, user_id) = st.socid[1:].split('@', 1)
        if '.' not in site_id and '.' in user_id:
            user_id, site_id = site_id, user_id
        if backwards:
            st.socid = f'@{site_id}@{user_id}'
        else:
            st.socid = f'@{user_id}@{site_id}'
        st.save()

def backwards_usersocs(apps, schema_editor):
    migrate_usersocs(apps, schema_editor, backwards=True)

class Migration(migrations.Migration):

    dependencies = [
        ('person', '0036_auto_20221107_2134'),
    ]

    operations = [
        migrations.RunPython(migrate_usersocs, backwards_usersocs),
    ]

